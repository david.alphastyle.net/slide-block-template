/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';


const { MediaUpload, InspectorControls } = wp.blockEditor;
const { Button } = wp.components;
/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-block-editor/#useblockprops
 */

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/#edit
 *
 * @return {Element} Element to render.
 */
export default function Edit({ attributes, setAttributes }) {
	const { slides } = attributes;

	return (
		<div>
			<InspectorControls>
				{/* Add controls for slide settings */}
			</InspectorControls>
			{slides.map((slide, index) => (
				<div key={index} style={{ border: '1px solid #ddd', padding: '10px', marginBottom: '10px' }}>
					<MediaUpload
						onSelect={(media) => {
							const newSlides = [...slides];
							newSlides[index].image = media.url;
							setAttributes({ slides: newSlides });
						}}
						type="image"
						value={slide.image}
						render={({ open }) => (
							<>
								<Button onClick={open}>Choose Image</Button>
								{slide.image && (
									<img
										src={slide.image}
										alt="Selected Thumbnail"
										style={{ maxWidth: '100px', marginTop: '10px' }}
									/>
								)}
							</>
						)}
					/>

					{/* Add a link input for each slide */}
					<input
						type="text"
						value={slide.link}
						onChange={(e) => {
							const newSlides = [...slides];
							newSlides[index].link = e.target.value;
							setAttributes({ slides: newSlides });
						}}
						placeholder="Liên kết Slide"
						style={{ display: 'block', marginTop: '10px' }}
					/>

					{/* Delete button */}
					<Button
						isDestructive
						onClick={() => {
							const newSlides = slides.filter((_, i) => i !== index);
							setAttributes({ slides: newSlides });
						}}
						style={{ marginTop: '10px' }}
					>
						Xóa Slide
					</Button>
				</div>
			))}
			<Button
				onClick={() => {
					setAttributes({ slides: [...slides, { image: '', link: '' }] });
				}}
				style={{ marginTop: '20px' }}
			>
				Tạo Slide mới
			</Button>
		</div>
	);
}
