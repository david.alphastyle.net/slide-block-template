import { useBlockProps } from '@wordpress/block-editor';

export default function save({ attributes }) {
  const { slides } = attributes;

  // Generate the HTML structure for each slide.
  const slideList = slides.map((slide, index) => (
    <li className="splide__slide" key={index}>
      <a href={slide.link} target="_blank" rel="noopener noreferrer">
        <img class="full-width-image" src={slide.image} alt="Slide" />
      </a>
    </li>
  ));

  // Create the entire Splide structure.
  return (
    <div className="splide">
      <div className="splide__track">
        <ul className="splide__list">
          {slideList}
        </ul>
      </div>
    </div>
  );
}
